import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProjectInput } from './dto/create-project.input';
import { UpdateProjectInput } from './dto/update-project.input';
import { Project } from './entities/project.entity';

@Injectable()
export class ProjectService {

  constructor(
    @InjectRepository(Project) private projectRepo : Repository<Project>
   ){}

  create(createProjectInput: CreateProjectInput)  {
    return this.projectRepo.save(createProjectInput)
  }

  async findAll() : Promise<Project[]> {
    return this.projectRepo.find(
      {
        relations : ["employees"]
      }
    )
  }

  findOne(id: string) : Promise<Project> {
    return this.projectRepo.findOne(id , {
      relations : ['employees']
    })
  }

  update(id: string , updateProjectInput: UpdateProjectInput) {
    return this.projectRepo.update( id , updateProjectInput )
  }

  remove(id: string) {
    return this.remove(id)
  }
}
