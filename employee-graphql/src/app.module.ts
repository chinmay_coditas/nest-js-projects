import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { dbConfig } from './db.config';

import { EmployeeModule } from './employee/employee.module';
import { ProjectModule } from './project/project.module';

@Module({
  imports: [EmployeeModule ,
    GraphQLModule.forRoot({
      autoSchemaFile: join(process.cwd(), 'src/graphql-schema.gql'),
    }) , 
    TypeOrmModule.forRoot(dbConfig  ), ProjectModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
