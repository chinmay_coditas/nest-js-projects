import { TypeOrmModule, TypeOrmModuleOptions } from "@nestjs/typeorm";
import { Employee } from "./employee/entities/employee.entity";
import { Project } from "./project/entities/project.entity";



export const dbConfig : TypeOrmModuleOptions = {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'postgres',
    password: 'Chinmay',
    database: 'employee_graphql',
    entities : [ Employee  , Project ] ,
    synchronize: true, // should be false on production
}