import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { TeacherController } from './teachers.controllers';
import { StudentTeacherController } from 'src/teachers/students.controller';
import { TeachersService } from './teachers.service';
import { StudentsService } from 'src/students/students.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Teachers } from './teachers.entity';
import { Students } from 'src/students/students.entity';
import { ValidTeacherMiddleware, ValisStuTeaMiddleware } from 'src/common/middleware/validTeacher.middleware';
    

@Module({
    imports : [ TypeOrmModule.forFeature( [Teachers , Students] ) ] , 
    controllers : [ TeacherController , StudentTeacherController ] , 
    providers : [  TeachersService , StudentsService ]
})
export class TeachersModule implements NestModule {
    configure( consumer : MiddlewareConsumer ) {
        consumer.apply( ValidTeacherMiddleware ).forRoutes( {
            path : "/teachers/:teacherId/students" , 
            method : RequestMethod.GET
        } ) , 
        consumer.apply( ValidTeacherMiddleware ).forRoutes( {
            path : "/teachers/:teacherId/students" , 
            method : RequestMethod.POST
        } ) , 
        consumer.apply( ValisStuTeaMiddleware ).forRoutes( {
            path : "/teachers/:teacherId/students/:studentId" , 
            method : RequestMethod.PUT
        } )
    }
}
