import { HttpException, Injectable, NestMiddleware, NestModule } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Request, Response, NextFunction } from "express";
import { Teachers } from "src/teachers/teachers.entity";
import { Students } from "src/students/students.entity";
import { UpdateStudentDto } from "src/students/dto/students.dto";





@Injectable()
export class ValidTeacherMiddleware implements NestMiddleware {

    constructor(
        @InjectRepository(Teachers)
        private TeacherRepo: Repository<Teachers>,
    ) { }

    async use(req: Request, res: Response, next: NextFunction) {

        const teacherId = req.params.teacherId
        const teacherExits = this.TeacherRepo.findOneOrFail(teacherId)
        try {
            await teacherExits
            next()
        }
        catch {
            throw new HttpException("Incorrect Teacher Id ", 400)
        }
    }

}

export class ValisStuTeaMiddleware implements NestMiddleware {
    constructor(
        @InjectRepository(Students)
        private StudentRepo: Repository<Students>,
    ) { }

    async use(req: Request, res: Response, next: NextFunction) {
        const teacherId = req.params.teacherId
        const studentId = req.params.studentId
        const pairExits = this.StudentRepo.findOneOrFail({
            where: {
                teacher_id: teacherId ,
                student_id: studentId
            }
        })

        try {
            await pairExits
            next()
        }
        catch {
            throw new HttpException("Incorrect Teacher Id or StudentId", 400)
        }
    }
}