
export class UpdateStudentDto {
    student_name: string 
    marks : number
}

export class CreateStudentDto {
    student_name : string 
    marks : number
}