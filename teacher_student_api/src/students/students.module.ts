import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ValidStudentMiddleware } from 'src/common/middleware/validStudent.middleware';
import { StudentController } from 'src/students/students.controllers';
import { Students } from './students.entity';
import { StudentsService } from './students.service';


@Module({
    imports : [ TypeOrmModule.forFeature( [Students] ) ] , 
    controllers : [ StudentController ] , 
    providers : [ StudentsService  ] 
})
export class StudentsModule implements NestModule{
    configure( consumer : MiddlewareConsumer ) {
        consumer.apply( ValidStudentMiddleware ).forRoutes(  {
            path : "/students/:studentId" , 
            method : RequestMethod.GET
        }) , 
        consumer.apply( ValidStudentMiddleware ).forRoutes(  {
            path : "/students/:studentId" , 
            method : RequestMethod.PUT
        })
    }
} // adding middleware
