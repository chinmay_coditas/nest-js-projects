import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { Roles } from 'src/user/user.interface';
import { Roles_Key } from './roles.decorator';

@Injectable()
export class RolesGuard implements CanActivate {

  constructor (
    private reflector : Reflector
  ){}

 

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const requiredRoles = this.reflector.getAllAndOverride<Roles[]>(Roles_Key , [
      context.getHandler() , 
      context.getClass()
    ] )

    if ( ! requiredRoles ) {
      return true
    } 
    const {  user } =   context.switchToHttp().getRequest()
    return requiredRoles.some( rol => 
      user.role?.includes(rol)  
      )
  }

}
