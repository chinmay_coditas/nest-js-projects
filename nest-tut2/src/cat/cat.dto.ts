import { ApiProperty } from "@nestjs/swagger";
import { IsString , IsInt, IsOptional, IsUUID } from "class-validator";


export class createCatDto {

    @ApiProperty( { required: false } )
    @IsOptional()
    readonly cat_id! : number


    @ApiProperty({
        description: "name of cat" , 
        required : true
    })
    @IsString()
    readonly cat_name : string

    @ApiProperty({
        description: "age of cat" , 
        required : true
    })
    @IsInt()
    readonly cat_age : number

    @ApiProperty({
        description: "breed of cat" , 
        required : true
    })
    @IsString()
    readonly cat_breed : string


}

