export interface ICat {
    cat_id : number ,
    cat_name : string , 
    cat_age : number , 
    cat_breed : string 
}