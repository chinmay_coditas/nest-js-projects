import { Body, CacheInterceptor, Controller, Delete , Get, HttpCode, HttpStatus, Param, Post, Request, RequestMethod, Response, UploadedFile, UseGuards, UseInterceptors, UsePipes, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import {  response } from 'express';
import { RoleDeco } from 'src/auth/roles.decorator';
import { RolesGuard } from 'src/auth/roles.guard';
import { CacheKey } from 'src/common/cache.decorator';
import { ChacheManagerInterceptor } from 'src/common/chache-manager.interceptor';
import { Roles } from 'src/user/user.interface';
import { createCatDto } from './cat.dto';
import { ICat } from './cat.interface';
import { CatService } from './cat.service';

@Controller('cat')
@UseInterceptors(  ChacheManagerInterceptor )
@UseGuards( AuthGuard('jwt') , RolesGuard )
@UsePipes(
    new ValidationPipe( {
        whitelist : true , 
        transform : true
    } )
)
export class CatController {
    constructor(
        private readonly catService : CatService
    ) {}

   @RoleDeco(Roles.Admin , Roles.EDITOR)
    @Post()
    @CacheKey("create_cat")
    async createCat(
        @Body() cat : createCatDto , 
            ): Promise<ICat> {
        return this.catService.create(cat)  
    }

   
    @Get()
    @RoleDeco(Roles.Admin)
    @CacheKey("get_all")
    async getAll(
        @Request() req
    ) : Promise<ICat[]> {
        return this.catService.findAll()
    }

    @Get("/:id")

    @UsePipes(ValidationPipe)
    async getById( 
        @Param("id") cat_id : number
    ) : Promise<ICat> {
        return this.catService.getById(cat_id)
    }

    @Post("/upload/:id")
    @UseInterceptors(FileInterceptor('image'))
    async uploadFile(
        @UploadedFile() file : Express.Multer.File , 
        @Param("id") cat_id : number ,
        @Response() res : Response 
    ) {
        file.filename = `cat-${cat_id}`
    }


}


