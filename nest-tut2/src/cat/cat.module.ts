import {  CacheInterceptor, MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthMiddleware } from '../common/auth.middleware';
import { Cat } from './cat.entity';
import { CatController } from './cat.controller';
import { CatService } from './cat.service';
import { JwtStrategy } from 'src/auth/jwt.strategy';
import { ChacheManagerInterceptor } from 'src/common/chache-manager.interceptor';
import { MulterModule } from '@nestjs/platform-express';

@Module({
    imports : [ AuthMiddleware  , TypeOrmModule.forFeature ([Cat])  , ChacheManagerInterceptor , MulterModule.register( {
        dest : './files'
    } ) ] , 
    controllers : [ CatController ] , 
    providers : [CatService , JwtStrategy ]

})
export class CatModule implements NestModule {
    public configure( consumer : MiddlewareConsumer ) {
        consumer.apply( AuthMiddleware)
        .forRoutes( {
            path : "/cat" ,
            method : RequestMethod.GET
        } )
    }
  
}
