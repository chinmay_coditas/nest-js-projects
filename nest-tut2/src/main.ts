import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger"


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const options = new DocumentBuilder().setTitle( " Cat Api " )
  .setDescription( "Cat Api for Learning " ).build()

  const doc = SwaggerModule.createDocument( app , options )
  SwaggerModule.setup(  "api" , app , doc)
  
  await app.listen(3000);
}
bootstrap();
