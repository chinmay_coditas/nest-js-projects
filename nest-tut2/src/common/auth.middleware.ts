import { HttpException, Injectable, NestMiddleware } from "@nestjs/common";
import { Request , Response ,  NextFunction } from "express";

@Injectable()
export class AuthMiddleware implements NestMiddleware {

    constructor (  ) {}
    async use ( req: Request , res : Response  , next : NextFunction ) {
        const { authorization } = req.headers
    if ( !authorization )  {
        throw new HttpException( " Unauthorized Request" , 400 )
    }

    // here we will validate token 
    next()
  }
}