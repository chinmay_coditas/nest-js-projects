import { HttpException, HttpStatus } from "@nestjs/common";


export class NotFoundException extends HttpException {
    constructor() {
        super ( 'Not Found'  , HttpStatus.NOT_FOUND )
    }
}

export class InvalidCred extends HttpException {
    constructor() {
        super ( ' Invalid Credentials '  , HttpStatus.NOT_FOUND )
    }
}