import { Body, Controller, Get, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { LoginDto, UserDto } from './user.dto';
import { IUser } from './user.interface';
import { UserService } from './user.service';


@Controller('user')
@UsePipes(
    new ValidationPipe( {
        whitelist : true , 
        transform : true
    } )
)
export class UserController {

    constructor ( 
        readonly userService : UserService
    ) {}

    @Post()
    @UsePipes(ValidationPipe)
    addUser( 
        @Body() user : UserDto
    ) : Promise<any> {
        return this.userService.addUser(user)
    }

    @Post("login")
    @UsePipes(ValidationPipe)
    loginUser(
        @Body() userCred : LoginDto
    ) : any  {
        return this.userService.loginUser(userCred)
    }

    @Get()
    getAll() : Promise<IUser[]> {
        return this.userService.getUser()
    } 
}
