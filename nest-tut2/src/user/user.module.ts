import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserController } from './user.controller';
import { UserEntity } from './user.entity';
import { UserService } from './user.service';

@Module({
  imports : [ TypeOrmModule.forFeature ( [ UserEntity ] )  , JwtModule.register( {
    secret : "Chinmay"
  } ) ] , 
  controllers: [UserController],
  providers: [UserService]
})
export class UserModule {}
