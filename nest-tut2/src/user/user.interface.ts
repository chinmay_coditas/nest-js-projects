export enum Roles { 
    Admin = 'Admin' ,
    EDITOR = 'Editor' , 
    SUBEDITOR = 'Sub-Editor' , 
}

export interface IUser {
    user_id : number , 
    username : string , 
    password : string , 
    role : Roles
}

export interface IUserCred {
    username : string , 
    password : string
}