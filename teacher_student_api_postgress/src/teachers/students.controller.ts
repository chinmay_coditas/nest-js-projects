import { Body, Controller, Get, Param, ParseIntPipe, ParseUUIDPipe, Post, Put} from "@nestjs/common";
import { CreateStudentDto, UpdateStudentDto } from "src/students/dto/students.dto";
import { Students } from "src/students/students.entity";
import { StudentsService } from '../students/students.service';
@Controller("teachers/:teacherId/students")
export class StudentTeacherController {


    constructor ( private readonly studentsService : StudentsService ) {}
    @Get()
    async getParticularStudent(
        @Param("teacherId") teacherId : number 
    ) : Promise<Students[]> {
        return this.studentsService.getParticularStudent(teacherId)
    }
    
    @Post()
    async createStudent ( 
        @Param("teacherId" , ParseIntPipe) teacherId : number ,
        @Body() data : CreateStudentDto
    )  {
        return this.studentsService.createStudent( teacherId , data )
    }

    @Put("/:studentId")
    async updateStudent (
        @Param( "studentId" , ParseIntPipe  ) studentId : number  ,
        @Body() data : UpdateStudentDto
    ) {
        return this.studentsService.updateStudent( studentId ,data)
    }
} 