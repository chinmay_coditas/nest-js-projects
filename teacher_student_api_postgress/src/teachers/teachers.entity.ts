import { Students } from "src/students/students.entity";
import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity("teachers")
export class Teachers extends BaseEntity {
    @PrimaryGeneratedColumn()
    teacher_id : number 

    @Column({ type : "varchar" })
    teacher_name : string

   @OneToMany( () => Students , stu => stu.teacher )
   stu : Students[]
}