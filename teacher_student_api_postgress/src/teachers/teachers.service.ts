import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Teachers } from './teachers.entity';
import { Repository } from 'typeorm';

@Injectable()
export class TeachersService { 
    constructor (  
        @InjectRepository(Teachers)
        private TeacherRepo : Repository<Teachers> ,
    ){}

    async getTeachers(): Promise<Teachers[]> {
        return this.TeacherRepo.find()
    }

    async getTeacherById( teacherId : string): Promise<Teachers> {
        try {   
            const teacher = this.TeacherRepo.findOneOrFail( teacherId )
            return teacher
        }
        catch  (err){
            throw err 
        }
    }
}
