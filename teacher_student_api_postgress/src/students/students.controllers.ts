// how to specify controllers using decorators 

import { Body, Controller, Get, Param  , Post, Put , ParseUUIDPipe, ParseIntPipe} from "@nestjs/common"
import { UpdateStudentDto } from "./dto/students.dto"
import { Students } from "./students.entity"
import { StudentsService } from "./students.service"


@Controller("students") // every single route will start with 'student'
export class StudentController {
    // all routes will be specified here

    constructor(private readonly studentSerivce : StudentsService ) {}
    @Get() // get req route will be : get /student
    async getStudents() : Promise<Students[]> {
        return this.studentSerivce.getStudents()
    }

    @Get("/:studentId")
    async getStudentById(
        @Param( "studentId"  ) student_id : number  
    ) : Promise<Students> {
        return this.studentSerivce.getStudentById(student_id)
    }

    @Put("/:studentId")
    async updateStudent (
        @Param( "studentId" , ParseIntPipe  ) studentId : number  ,
       @Body () data : UpdateStudentDto
    ) : Promise<Students> {
        return this.studentSerivce.updateStudent( studentId , data)
    }
}